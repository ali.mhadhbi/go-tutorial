package main

import (
	"fmt"
	"reflect"
)

/*
These are the following rules for naming a Golang variable:
	- A name must begin with a letter, and can have any number of additional letters and numbers.
	- A variable name cannot start with a number.
	- A variable name cannot contain spaces.
	- If the name of a variable begins with a lower-case letter,
	it can only be accessed within the current package this is considered as unexported variables.
	- If the name of a variable begins with a capital letter,
	it can be accessed from packages outside the current package one this is considered as exported variables.
	- If a name consists of multiple words,
	each word after the first should be capitalized like this: empName, EmpAddress, etc.
	- Variable names are case-sensitive (car, Car and CAR are three different variables).
*/

// DeclarationWithInitialization
/*
	If you know beforehand what a variable's value will be,
	you can declare variables and assign them values on the same line.
*/
func DeclarationWithInitialization() {
	fmt.Println("[X] Declaration With Initialization [X]")
	var i int = 10
	var s string = "Canada"

	fmt.Println("i : ", i)
	fmt.Println("s : ", s)
}

// DeclarationWithoutInitialization
/*
	The keyword var is used for declaring variables followed by
	the desired name and the type of value the variable will hold.
*/
func DeclarationWithoutInitialization() {
	fmt.Println("[X] Declaration Without Initialization [X]")
	var i int
	var s string

	i = 10
	s = "Canada"

	fmt.Println("i : ", i)
	fmt.Println("s : ", s)
}

// DeclarationWithTypeInference
/*
	You can omit the variable type from the declaration,
	when you are assigning a value to a variable at the time of declaration.
	The type of the value assigned to the variable will be used as the type of that variable.
*/
func DeclarationWithTypeInference() {
	fmt.Println("[X] Declaration with type inference [X]")
	var i = 10
	var s = "Canada"

	fmt.Println("Type of variable i : ", reflect.TypeOf(i))
	fmt.Println("Type of variable s : ", reflect.TypeOf(s))
}

// DeclarationOfMultipleVariables
/*
	Golang allows you to assign values to multiple variables in one line.
*/
func DeclarationOfMultipleVariables() {
	fmt.Println("[X] Declaration of multiple variables [X]")
	var fName, lName string = "John", "Doe"
	m, n, o := 1, 2, 3
	item, price := "Mobile", 2000

	fmt.Println("First and last name : ", fName, "", lName)
	fmt.Println("SUM: ", m+n+o)
	fmt.Println("<item> - <price> : ", item, "-", price)
}

// DeclarationOfShortVariable
/*
	The := short variable assignment operator indicates that short variable declaration is being used.
	There is no need to use the var keyword or declare the variable type.
*/
func DeclarationOfShortVariable() {
	fmt.Println("[X] Short Variable Declaration in Golang [X]")
	name := "John Doe"
	fmt.Println("Type of variable name : ", reflect.TypeOf(name))

}

func main() {
	DeclarationWithInitialization()
	DeclarationWithoutInitialization()
	DeclarationWithTypeInference()
	DeclarationOfMultipleVariables()
	DeclarationOfShortVariable()
}
